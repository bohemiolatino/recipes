class AddBioToChefs < ActiveRecord::Migration[5.1]
  def change
    add_column :chefs, :bio, :text
  end
end
